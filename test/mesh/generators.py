import numpy as np
import fetk.mesh.generators as meshgen


def test_rectilinear2d():
    nodes, elements = meshgen.rectilinear2d(2, 2, 4, 4)
    assert np.allclose(
        nodes,
        [
            [1, 0.0, 0.0],
            [2, 2.0, 0.0],
            [3, 4.0, 0.0],
            [4, 0.0, 2.0],
            [5, 2.0, 2.0],
            [6, 4.0, 2.0],
            [7, 0.0, 4.0],
            [8, 2.0, 4.0],
            [9, 4.0, 4.0],
        ],
    )
    assert np.allclose(
        elements, [[1, 1, 2, 5, 4], [2, 2, 3, 6, 5], [3, 4, 5, 8, 7], [4, 5, 6, 9, 8]]
    )
