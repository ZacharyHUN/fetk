import numpy as np
import fetk.element
import fetk.material


def test_3d_hex():
    """ """
    xc = np.array(
        [
            [0, 0, 0],
            [1, 0, 0],
            [1, 1, 0],
            [0, 1, 0],
            [0, 0, 1],
            [1, 0, 1],
            [1, 1, 1],
            [0, 1, 1],
        ],
        dtype=float,
    )
    material = fetk.material.elastic({"E": 96.0, "nu": 1.0 / 3.0})
    element = fetk.element.C3D8F()
    ke = element.stiffness(xc, material)
    a, c, e, f, g, h, z = 24.0, 9.0, 3.0, 1.5, 6.0, 4.5, 0
    C, D, E, F, G, H = -9.0, -12.0, -3.0, -1.5, -6.0, -4.5
    expected = np.array(
        [
            [a, c, c, D, e, e, C, C, f, g, E, h, g, h, E, C, f, C, G, H, H, z, F, F],
            [c, a, c, E, g, h, C, C, f, e, D, e, h, g, E, F, z, F, H, G, H, f, C, C],
            [c, c, a, E, h, g, F, F, z, h, E, g, e, e, D, C, f, C, H, H, G, f, C, C],
            [D, E, E, a, C, C, g, e, H, C, c, F, C, F, c, g, H, e, z, f, f, G, h, h],
            [e, g, h, C, a, c, E, D, e, c, C, f, f, z, F, H, g, E, F, C, C, h, G, H],
            [e, h, g, C, c, a, H, E, g, f, F, z, c, f, C, E, e, D, F, C, C, h, H, G],
            [C, C, F, g, E, H, a, c, C, D, e, E, G, H, h, z, F, f, g, h, e, C, f, c],
            [C, C, F, e, D, E, c, a, C, E, g, H, H, G, h, f, C, c, h, g, e, F, z, f],
            [f, f, z, H, e, g, C, C, a, e, H, g, h, h, G, F, c, C, E, E, D, c, F, C],
            [g, e, h, C, c, f, D, E, e, a, C, c, z, f, F, G, h, H, C, F, C, g, H, E],
            [E, D, E, c, C, F, e, g, H, C, a, C, F, C, c, h, G, h, f, z, f, H, g, e],
            [h, e, g, F, f, z, E, H, g, c, C, a, f, c, C, H, h, G, C, F, C, e, E, D],
            [g, h, e, C, f, c, G, H, h, z, F, f, a, c, C, D, e, E, C, C, F, g, E, H],
            [h, g, e, F, z, f, H, G, h, f, C, c, c, a, C, E, g, H, C, C, F, e, D, E],
            [E, E, D, c, F, C, h, h, G, F, c, C, C, C, a, e, H, g, f, f, z, H, e, g],
            [C, F, C, g, H, E, z, f, F, G, h, H, D, E, e, a, C, c, g, e, h, C, c, f],
            [f, z, f, H, g, e, F, C, c, h, G, h, e, g, H, C, a, C, E, D, E, c, C, F],
            [C, F, C, e, E, D, f, c, C, H, h, G, E, H, g, c, C, a, h, e, g, F, f, z],
            [G, H, H, z, F, F, g, h, E, C, f, C, C, C, f, g, E, h, a, c, c, D, e, e],
            [H, G, H, f, C, C, h, g, E, F, z, F, C, C, f, e, D, e, c, a, c, E, g, h],
            [H, H, G, f, C, C, e, e, D, C, f, C, F, F, z, h, E, g, c, c, a, E, h, g],
            [z, f, f, G, h, h, C, F, c, g, H, e, g, e, H, C, c, F, D, E, E, a, C, C],
            [F, C, C, h, G, H, f, z, F, H, g, E, E, D, e, c, C, f, e, g, h, C, a, c],
            [F, C, C, h, H, G, c, f, C, E, e, D, H, E, g, f, F, z, e, h, g, C, c, a],
        ]
    )
    assert np.allclose(ke, expected)
