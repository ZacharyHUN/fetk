=====================================
Programming the Finite Element Method
=====================================

``fetk`` is a python package providing procedures for solving problems using the
finite element method.  ``fetk`` is intended to be used in a university finite
element course and focuses on code clarity over speed and efficiency.  The
problems that can be solved are relatively simple when compared to commercial
finite element programs.  ``fetk`` contains a relatively small library of
elements and list of material models.

Full Table of Contents
----------------------

.. toctree::
   :numbered:
   :maxdepth: 2

   intro/index
   nodes/index
   elements/index
   element_blocks/index
   boundary/index
   dload/index
   materials/index
   input/index


==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
