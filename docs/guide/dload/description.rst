===================
General Description
===================

Distributed loads specify the element ID, the degree of freedom being loaded, and the magnitude of the load.  Distributed loads can be used to describe both mechanical and thermal sources.  The element ID is the *external* element number, the degree of freedom is 0 for :math:`x`, 1 for :math:`y`, and 2 for :math:`z`.  The magnitude is the value of the applied load.
