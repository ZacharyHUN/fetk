============================
What's unique about ``fetk``
============================

The present report discusses an implementation of the finite element method
using Python. The implementation is written as a set of modules
collectively call ``fetk`` which is an acronym for Python Finite Element Toolkit.

``fetk`` provides the following unique features:

1. A strict treatment of degrees of freedom. The minimum number of freedoms at
   each node needed to solve the problem is automatically used. For example, if
   the model contains only flat plate bending elements, three degrees of freedom
   are carried at each node. If some nodes of the model require an additional
   freedom, that freedom is carried there and nowhere else. This approach is
   made possible because of the use of list structures.

2. The toolkit approach. The user builds custom FEM program by calling toolkit
   functions. No unique closed program is provided; just examples. Source code
   is always available, and the user is encouraged to write own contributions.
   This white-box approach ensures that programs can always contain the latest
   technology, avoiding the obsolescence typical of finite element black boxes.
