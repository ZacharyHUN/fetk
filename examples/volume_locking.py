#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

from fetk.io import exodusii
from fetk.io import plot2d
from fetk.fe_model import fe_model
from fetk.material import elastic
from fetk.element import C2D4E, C2D4EF


mu = 1.0
Nu = 0.499
E = 2.0 * mu * (1.0 + Nu)


def fully_integrated_solution():
    mesh = exodusii.load("quarter_cylinder.g")

    model = fe_model("fully_integrated", mesh.nodes, mesh.elements)
    model.element_block(
        "block-1",
        elements="all",
        element=C2D4EF(thickness=1.0),
        material=elastic({"E": E, "Nu": Nu}),
    )
    for (name, nodes) in mesh.nodesets.items():
        model.nodeset(name, nodes=nodes)
    for (name, sides) in mesh.sidesets.items():
        model.sideset(name, sides=sides)

    model.boundary("Nodeset-200", "x", 0.0)
    model.boundary("Nodeset-201", "y", 0.0)

    # Pressure on inside face
    model.pressure("Surface-1", 1)
    model.solve()
    model.dump(format="exodus", disp=model.solution.u)
    return model


def sri_solution():
    mesh = exodusii.load("quarter_cylinder.g")

    model = fe_model("sri", mesh.nodes, mesh.elements)
    model.element_block(
        "block-1",
        elements="all",
        element=C2D4E(thickness=1.0),
        material=elastic({"E": E, "Nu": Nu}),
    )
    for (name, nodes) in mesh.nodesets.items():
        model.nodeset(name, nodes=nodes)
    for (name, sides) in mesh.sidesets.items():
        model.sideset(name, sides=sides)

    model.boundary("Nodeset-200", "x", 0.0)
    model.boundary("Nodeset-201", "y", 0.0)

    # Pressure on inside face
    model.pressure("Surface-1", 1)
    model.solve()
    model.dump(format="exodus", disp=model.solution.u)
    return model


def analytic_solution():
    mesh = exodusii.load("quarter_cylinder.g")
    xc = np.zeros((len(mesh.nodes), 2))
    connect = np.zeros((len(mesh.elements), 4), dtype=int)
    for (i, row) in enumerate(mesh.nodes):
        xc[i] = row[1:]
    for (i, row) in enumerate(mesh.elements):
        connect[i] = row[1:]
    a = xc[0, 1]
    b = xc[-1, 0]
    p = 1.0
    u = np.zeros_like(xc)
    for (i, x) in enumerate(xc):
        r = np.sqrt(x[0] ** 2 + x[1] ** 2)
        term1 = (1.0 + Nu) * a ** 2 * b ** 2 * p / (E * (b ** 2 - a ** 2))
        term2 = 1.0 / r + (1.0 - 2.0 * Nu) * r / b ** 2
        ur = term1 * term2
        u[i, :] = ur * x[:] / r
    exodusii.put_nodal_solution("analytic", xc, connect - 1, u)
    return u


def plot_overlay(analytic, fem):
    xc = fem.coord
    connect = fem.blocks[0].connect
    u1 = analytic
    u2 = fem.solution.u
    plt.clf()
    plt.cla()
    ax = plot2d.plot2d(xc + u1, connect, color="orange", weight=8, label="Analytic")
    plot2d.plot2d(
        xc,
        connect,
        u=u2,
        color="b",
        linestyle="-.",
        ax=ax,
        label=fem.name.title(),
        show=1,
    )


def main():
    analytic = analytic_solution()
    full = fully_integrated_solution()
    sri = sri_solution()

    plot_overlay(analytic, full)
    plot_overlay(analytic, sri)


if __name__ == "__main__":
    main()
