import numpy as np
import matplotlib.pyplot as plt

import fetk.element
import fetk.material
import fetk.fe_model
from fetk.io import plot2d
import fetk.mesh.generators as mgen


G, Nu = 10000.0, 0.0
E = 2 * G * (1 + Nu)


def main():

    nx, ny = 10, 3
    height = 0.3
    length = 10.0
    model = fem_solution(a=height / 2, L=length, nx=nx, ny=ny)
    ua = analytic_solution(a=height / 2, L=length, nx=nx, ny=ny)

    overlay(model, ua, scale=1)


def overlay(model, analytic, scale=1):

    xc = model.coord
    connect = model.blocks[0].connect
    u = model.solution.u

    ax = plot2d.plot2d(xc + scale * u, connect, color="blue", weight=0.5, label="FEM")
    plot2d.plot2d(
        xc + scale * analytic,
        connect,
        ax=ax,
        color="red",
        weight=0.5,
        linestyle="-.",
        label="Analytic",
        show=1,
    )


def fem_solution(L=10.0, a=0.15, nx=10, ny=3, b=1):

    nodes, elements = mesh(nx=nx, ny=ny, L=L, a=a)
    material = fetk.material.elastic({"E": E, "Nu": Nu})
    model = fetk.fe_model.fe_model("incompatible_modes", nodes, elements)
    element = fetk.element.factory("C2D4S", thickness=1.0)
    model.element_block("Block-1", elements="all", element=element, material=material)

    # Fix x and y displacements on nodes at x == L and y == 0
    nodes_ns = model.find_nodes(lambda x: (x[:, 0] == L) & (x[:, 1] == 0))
    model.nodeset("Nodeset-1", nodes=nodes_ns)
    model.boundary("Nodeset-1", "xy", 0.0)

    # Fix x displacements on nodes at x == L and y > 0
    nodes_ns = model.find_nodes(lambda x: (x[:, 0] == L) & (x[:, 1] > 0))
    model.nodeset("Nodeset-2", nodes=nodes_ns)
    model.boundary("Nodeset-2", "x", 0.0)

    # Apply surface traction in y direction on faces at x == 0
    sides = model.find_sides2d(lambda x: x[:, 0] == 0)
    model.sideset("Sideset-1", sides=sides)
    model.surface_force("Sideset-1", [0.0, -1.0])

    model.solve()
    return model


def analytic_solution(L=10, a=0.15, nx=10, ny=3, b=1):
    b = 1
    nodes, _ = mesh(nx=nx, ny=ny, L=L, a=a)
    xc = np.array([row[1:] for row in nodes])
    u = np.zeros_like(xc)
    P = 2 * a * b
    for (i, row) in enumerate(xc):
        x, y = row[0], row[1] - a
        II = b * (2 * a) ** 3 / 12
        ux = (
            P / (2 * E * II) * x ** 2 * y
            + Nu * P / (6 * E * II) * y ** 3
            - P / (6 * II * G) * y ** 3
            - (P * L ** 2 / (2 * E * II) - P * a ** 2 / (2 * II * G)) * y
        )
        uy = (
            -Nu * P / (2 * E * II) * x * y ** 2
            - P / (6 * E * II) * x ** 3
            + P * L ** 2 / (2 * E * II) * x
            - P * L ** 3 / (3 * E * II)
        )
        u[i, :] = [ux, uy]
    return u


def mesh(nx=10, ny=3, L=10.0, a=0.15):
    return mgen.rectilinear2d(nx=nx, ny=ny, lx=L, ly=2 * a)


if __name__ == "__main__":
    main()
