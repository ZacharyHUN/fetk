import os
import numpy as np
import fetk.mesh


def Plot2D(
    coord,
    xy=None,
    elecon=None,
    u=None,
    color=None,
    ax=None,
    show=0,
    weight=None,
    colorby=None,
    linestyle="-",
    label=None,
    xlim=None,
    ylim=None,
    **kwds
):
    assert coord.ndim == 2
    from matplotlib.patches import Polygon

    # import matplotlib.lines as mlines
    from matplotlib.collections import PatchCollection

    # from matplotlib.cm import coolwarm
    from matplotlib.cm import Spectral
    import matplotlib.pyplot as plt

    if xy is None:
        xy = np.array(coord)
    if u is not None:
        xy += u.reshape(xy.shape)

    patches = []
    for points in xy[elecon[:]]:
        quad = Polygon(points, True)
        patches.append(quad)

    if ax is None:
        fig, ax = plt.subplots()

    # colors = 100 * random.rand(len(patches))
    p = PatchCollection(patches, linewidth=weight, **kwds)
    if colorby is not None:
        colorby = np.asarray(colorby).flatten()
        if len(colorby) == len(xy):
            # average value in element
            colorby = np.array([np.average(colorby[points]) for points in elecon])
        p.set_cmap(Spectral)  # coolwarm)
        p.set_array(colorby)
        p.set_clim(vmin=colorby.min(), vmax=colorby.max())
        fig.colorbar(p)
    else:
        p.set_edgecolor(color)
        p.set_facecolor("None")
        p.set_linewidth(weight)
        p.set_linestyle(linestyle)

    if label:
        ax.plot([], [], color=color, linestyle=linestyle, label=label)

    ax.add_collection(p)

    if not ylim:
        ymin, ymax = np.amin(xy[:, 1]), np.amax(xy[:, 1])
        dy = max(abs(ymin * 0.05), abs(ymax * 0.05))
        ax.set_ylim([ymin - dy, ymax + dy])
    else:
        ax.set_ylim(ylim)

    if not xlim:
        xmin, xmax = np.amin(xy[:, 0]), np.amax(xy[:, 0])
        dx = max(abs(xmin * 0.05), abs(xmax * 0.05))
        ax.set_xlim([xmin - dx, xmax + dx])
    else:
        ax.set_xlim(xlim)
    ax.set_aspect("equal")

    if show:
        plt.show()
    return ax


def PlotScalar2D(coord, connect, u, show=0):
    import matplotlib.pyplot as plt

    # from mpl_toolkits.mplot3d import Axes3D
    from matplotlib.cm import Spectral

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection="3d")
    ax.plot_trisurf(coord[:, 0], coord[:, 1], u, triangles=connect, cmap=Spectral)
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    if show:
        plt.show()
    return


def PutNodalSolution(coord, connect, node_map, elem_map, blocks, filename, u):
    import exodusii

    numdim = coord.shape[0]
    if not blocks:
        eletyp = ElementFamily(numdim, len(connect[0]))
        blocks = [element_block("ElementBlock1", "all", eletyp)]
    if not filename.endswith((".exo", ".e")):
        filename += ".exo"
    exodusii.PutNodalSolution(filename, node_map, elem_map, coord, connect, blocks, u)


def VTU2Genesis(nodtab=None, eletab=None, filename=None):
    if filename is None:
        assert nodtab is not None and eletab is not None
        outfile = "mesh.g"
    elif not os.path.isfile(filename):
        assert nodtab is not None and eletab is not None
        assert filename.endswith(".g")
        outfile = filename
        filename = None
    else:
        assert nodtab is None and eletab is None
        outfile = os.path.splitext(filename)[0] + ".g"
    try:
        mesh = fetk.mesh.mesh(nodtab=nodtab, eletab=eletab, filename=filename)
    except KeyError:
        return
    mesh.to_genesis(outfile)
