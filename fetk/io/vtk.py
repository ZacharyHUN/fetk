#!/usr/bin/env python
import os
import gzip
import logging
import numpy as np
import xml.dom.minidom as xdom

import fetk.element as element


VTK_LINE = 3
VTK_BEAM = VTK_LINE
VTK_TRIANGLE = 5
VTK_QUAD = 9
VTK_TETRA = 10
VTK_HEXAHEDRON = 12
VTK_WEDGE = 13
VTK_PYRAMID = 14

VTK_S = "VTK"
VTK_USGRID = "UnstructuredGrid"
VTK_DATARR = "DataArray"
VTK_FILE = "VTKFile"

INDENT = 0


def arrtostr2(a, fmt=".18f", indent="", newl="\n"):
    a1 = ["{0}{1}".format(indent, arrtostr(row, fmt=fmt, newl="")) for row in a]
    return "{0}{1}{0}{2}".format(newl, newl.join(a1), indent[:-INDENT])


def arrtostr(a, fmt=".18f", newl="\n", indent=""):
    a1 = " ".join("{0:{1}}".format(x, fmt) for x in a)
    return "{0}{3}{1}{0}{2}".format(newl, a1, indent[:-INDENT], indent)


def vtk_cell_types(et):
    return {
        element.L1D2: VTK_LINE,
        element.L2D2: VTK_LINE,
        element.L3D2: VTK_LINE,
        element.B1D2: VTK_BEAM,
        element.B2D2: VTK_BEAM,
        element.C2D3: VTK_TRIANGLE,
        # H2D3: VTK_TRIANGLE,
        # C2D4: VTK_QUAD,
        # C3D4: VTK_TETRA,
        # C3D8: VTK_HEXAHEDRON,
        # C3D6: VTK_WEDGE,
        # C3D5: VTK_PYRAMID
    }[type(et)]


def pyfem_elem_type(et):
    return {
        VTK_LINE: element.L2D2,
        VTK_TRIANGLE: element.C2D3,
        # VTK_QUAD: element.C2D4,
        # VTK_TETRA: S3D4, VTK_HEXAHEDRON: S3D8,
        # VTK_WEDGE: S3D6, VTK_PYRAMID: S3D5
    }[et]


def arrtotens(a):
    if a.ndim == 1:
        if a.size == 4:
            return np.array((a[0], a[3], 0, a[3], a[1], 0, 0, 0, a[2]))
        elif a.size == 6:
            return np.array((a[0], a[3], a[5], a[3], a[1], a[4], a[5], a[4], a[2]))
    if a.shape[1] == 4:
        z = np.zeros(a.shape[0])
        return np.column_stack(
            (a[:, 0], a[:, 3], z, a[:, 3], a[:, 1], z, z, z, a[:, 2])
        )
    elif a.shape[1] == 6:
        return np.column_stack(
            (
                a[:, 0],
                a[:, 3],
                a[:, 5],
                a[:, 3],
                a[:, 1],
                a[:, 4],
                a[:, 5],
                a[:, 4],
                a[:, 2],
            )
        )
    raise ValueError("Unknown array size")


class vtk_file(object):
    def __init__(self, jobid=None, filename=None, mode="r"):
        self.jobid = jobid
        self.mode = mode.lower()
        self.pvd = None
        self.count = 0
        if mode not in "rw":
            raise ValueError("Unknown file mode")
        if self.mode[0] == "w":
            self.datadir = self.jobid + ".vtu"
            self.pvd = xdom.Document()
            self.pvd_root = self.pvd.createElementNS("VTK", "VTKFile")
            self.pvd_root.setAttribute("type", "Collection")
            self.pvd_root.setAttribute("version", "0.1")
            self.pvd_root.setAttribute("byte_order", "LittleEndian")
            self.pvd.appendChild(self.pvd_root)
            self.pvd_coll = self.pvd.createElementNS("VTK", "Collection")
            self.pvd_root.appendChild(self.pvd_coll)
        else:
            if filename is None:
                raise ValueError("No filename given")
            self.filename = filename
            x = self.readmesh(filename)
            (
                self.coords,
                self.node_label,
                self.connect,
                self.elem_label,
                self.elem_type,
            ) = x

    def put_init(self, coords, node_label, elem_label, elem_type, connect):
        self.coords = np.asarray(coords)
        self.node_label = np.asarray(node_label, dtype=int)
        self.connect = np.asarray(connect, dtype=int)
        self.elem_label = np.asarray(elem_label, dtype=int)
        self.elem_type = elem_type
        self.numnod, self.numdim = coords.shape
        self.numele, self.max_num_node_per_elem = connect.shape

    def readmesh(self, filename):
        if filename.endswith(".gz"):
            f = gzip.open(filename, "rb")
            self.doc = xdom.parseString(f.read())
            f.close()
        else:
            self.doc = xdom.parse(filename)
        root = self.doc.getElementsByTagName("VTKFile")[0]
        grid = root.getElementsByTagName("UnstructuredGrid")
        if not grid:
            logging.warn("No grid information found in {0!r}".format(filename))
            return None
        if len(grid) > 1:
            logging.warn("Multiple grids not supported")
            return None
        grid = grid[0]
        # Piece 0 (only one)
        piece = grid.getElementsByTagName("Piece")[0]
        numnod = int(piece.getAttribute("NumberOfPoints"))
        numele = int(piece.getAttribute("NumberOfCells"))
        points = piece.getElementsByTagName("Points")[0]
        el = points.getElementsByTagName("DataArray")[0]
        numcmp = int(el.getAttribute("NumberOfComponents"))
        numdim = int(el.getAttribute("NumberOfProblemDimensions"))
        s = [line for line in el.firstChild.data.split("\n") if line.split()]
        coords = np.array([float(a) for o in s for a in o.split()]).reshape(numnod, -1)
        assert coords.shape[1] == numcmp, "Error reading coords shape"
        coords = coords[:, :numdim]

        # Cells
        cells = piece.getElementsByTagName("Cells")[0]
        data = cells.getElementsByTagName("DataArray")
        for item in data:
            s = lambda el: [
                a
                for x in el.firstChild.data.split("\n")
                for a in x.split()
                if x.split()
            ]
            name = item.getAttribute("Name")
            if name == "connectivity":
                conn = np.array([int(x) for x in s(item)])
            elif name == "offsets":
                offsets = np.array([int(x) for x in s(item)])
            elif name == "types":
                elem_type = np.array([pyfem_elem_type(int(x)) for x in s(item)])
        maxnod = max(np.diff(offsets))

        # Field data
        fd = root.getElementsByTagName("FieldData")[0]

        # Node table
        node_labels = fd.getElementsByTagName("NodeLabels")[0]
        el = node_labels.getElementsByTagName("DataArray")[0]
        node_label = [
            int(a)
            for x in el.firstChild.data.split("\n")
            for a in x.split()
            if x.split()
        ]

        # Element table
        elem_labels = fd.getElementsByTagName("ElementLabels")[0]
        el = elem_labels.getElementsByTagName("DataArray")[0]
        elem_label = [
            int(a)
            for x in el.firstChild.data.split("\n")
            for a in x.split()
            if x.split()
        ]

        # format connectivity
        connect = np.zeros((numele, maxnod), dtype=int)
        k = 0
        for (el, offset) in enumerate(offsets):
            j = offset - k
            connect[el, :j] = [n for n in conn[k : k + j]]
            k += j

        return coords, node_label, connect, elem_label, elem_type

    def snapshot(self, disp=None, **kwds):
        if not os.path.isdir(self.datadir):
            os.makedirs(self.datadir)
        filename = os.path.join(
            self.datadir, self.jobid + "-{0:04d}.vtu".format(self.count)
        )
        self.write_vtu_file(filename=filename, disp=disp, **kwds)
        # Write the updated pvd file
        ds = self.pvd.createElementNS("VTK", "DataSet")
        ds.setAttribute("timestep", str(self.count))
        ds.setAttribute("group", "")
        ds.setAttribute("part", "0")
        ds.setAttribute("file", filename)
        self.pvd_coll.appendChild(ds)
        with open(self.jobid + ".pvd", "w") as fh:
            fh.write(self.pvd.toprettyxml(indent=""))
        self.count += 1

    def write_vtu_file(self, filename=None, disp=None, **kwds):
        filename = filename or self.jobid + ".vtu"
        self.doc = xdom.Document()
        root = self.doc.createElementNS("VTK", "VTKFile")
        root.setAttribute("type", "UnstructuredGrid")
        root.setAttribute("version", "0.1")
        root.setAttribute("byte_order", "LittleEndian")
        self.doc.appendChild(root)

        # unstructured grid
        self.write_grid(disp=disp)
        if disp is not None:
            kwds["u"] = disp

        # Data at nodes
        pd = self.create_element("PointData", parent="Piece")
        cd = self.create_element("CellData", parent="Piece")

        for (key, val) in kwds.items():
            val = np.asarray(val)
            da = self.doc.createElementNS("VTK", "DataArray")
            da.setAttribute("Name", key)
            if val.ndim == 1:
                # Scalar data
                nc = 1
                val = val.reshape(-1, 1)
            else:
                if val.shape[1] == self.numdim:
                    nc = 3
                    # Vector data
                    if val.shape[1] != 3:
                        z = np.zeros((self.numnod, 3 - val.shape[1]))
                        val = np.column_stack((val, z))
                elif val.shape[1] == 4:
                    val = arrtotens(val, 1)
                    nc = 6
                else:
                    nc = val.shape[1]

            da.setAttribute("NumberOfComponents", str(nc))
            da.setAttribute("type", "Float32")
            da.setAttribute("format", "ascii")
            if len(val) == self.numnod:
                pd.appendChild(da)
            elif len(val) == self.numele:
                cd.appendChild(da)
            else:
                raise Exception("unknown data shape")
            da.appendChild(self.doc.createTextNode(arrtostr2(val)))

        with open(filename, "w") as fh:
            fh.write(self.doc.toprettyxml(indent=""))

    def flatten(self, the_el=None):
        if the_el is None:
            the_el = self.doc
        flattened = []
        for el in the_el.childNodes:
            if el.nodeType != 1:
                continue
            flattened.append(el)
            flattened.extend(self.flatten(el))
        return flattened

    def create_element(self, name, parent="VTKFile", **kwds):
        el = self.doc.createElementNS("VTK", name)
        for (key, val) in kwds.items():
            el.setAttribute(key, str(val))
        for el1 in self.flatten():
            if el1.nodeName == parent:
                el1.appendChild(el)
                break
        else:
            raise ValueError("Parent node not found")
        return el

    def get_element(self, name, parent=None):
        for el in self.flatten():
            if el.nodeName == name:
                return el
        if parent is not None:
            return self.create_element(name, parent=parent)
        raise ValueError("No element {0!r}".format(name))

    def write_grid(self, disp=None):

        # write the unstructured grid

        # Unstructured grid element
        usgrid = self.create_element("UnstructuredGrid")

        # Piece 0 (only one)
        piece = self.create_element(
            "Piece",
            parent=usgrid.nodeName,
            NumberOfPoints=self.numnod,
            NumberOfCells=self.numele,
        )

        # Points
        points = self.create_element("Points", parent=piece.nodeName)

        # Point location data
        da = self.create_element(
            "DataArray",
            parent=points.nodeName,
            type="Float32",
            format="ascii",
            NumberOfComponents=3,
            NumberOfProblemDimensions=self.numdim,
        )
        x = np.array(self.coords)
        if disp is not None:
            x += disp
        if self.numdim != 3:
            z = np.zeros(self.numnod)
            for i in range(3 - self.numdim):
                x = np.column_stack((x, z))
        da.appendChild(self.doc.createTextNode(arrtostr2(x, indent="")))

        # Cells
        cells = self.create_element("Cells", parent=piece.nodeName)

        # Cell connectivity
        da = self.create_element(
            "DataArray",
            parent=cells.nodeName,
            type="Int32",
            Name="connectivity",
            format="ascii",
        )
        o = [
            [n for n in el[: self.elem_type[e].num_nodes]]
            for (e, el) in enumerate(self.connect)
        ]
        da.appendChild(self.doc.createTextNode(arrtostr2(o, "d", indent="")))

        # Cell offsets
        da = self.create_element(
            "DataArray",
            parent=cells.nodeName,
            type="Int32",
            Name="offsets",
            format="ascii",
        )
        o, k = [], 0
        for (e, el) in enumerate(self.connect):
            nn = self.elem_type[e].num_nodes
            k += nn
            o.append(k)
        da.appendChild(self.doc.createTextNode(arrtostr(o, "d", indent="")))

        # Cell types
        da = self.create_element(
            "DataArray",
            parent=cells.nodeName,
            type="UInt8",
            Name="types",
            format="ascii",
        )
        o = [vtk_cell_types(et) for et in self.elem_type]
        da.appendChild(self.doc.createTextNode(arrtostr(o, "d", indent="")))

        # Node and element tables
        self.create_fd("NodeLabels", "Int32", self.node_label)
        self.create_fd("ElementLabels", "Int32", self.elem_label)

        return

    def create_fd(self, name, dtype, arr):

        # Field data
        fd = self.get_element("FieldData", parent="VTKFile")

        el = self.create_element(name, parent=fd.nodeName)
        da = self.create_element(
            "DataArray", parent=el.nodeName, type=dtype, format="ascii"
        )
        arr = np.asarray(arr)
        fmt = {"Int32": "d", "Float32": ".18f"}[dtype]
        if arr.ndim == 1:
            string = arrtostr(arr, fmt, indent="")
        else:
            string = arrtostr2(arr, fmt, indent="")
        da.appendChild(self.doc.createTextNode(string))
        return

    def close(self):
        self.pvd.close()


def write_vtu_mesh(
    filename, coords, node_label, elem_label, elem_type, connect, check=0
):
    jobid = os.path.splitext(filename)[0]
    f = vtk_file(jobid, mode="w")
    f.put_init(coords, node_label, elem_label, elem_type, connect)
    f.write_vtu_file()
    if check:
        x, nl, ec, el, etyp = read(filename, disp=1)
        assert np.allclose(x, coords), "coords"
        assert np.allclose(nl, node_label), "node_label"
        assert np.allclose(ec, connect), "connect"
        assert np.allclose(el, elem_label), "elem_label"
        assert np.allclose(etyp, elem_type), "elem_type"


def snapshot(jobid, coords, elem_blocks, node_map, disp=None, **kwds):
    connect = []
    elem_type = []
    elem_map = {}
    iel = 0
    for block in elem_blocks:
        connect.extend(block.connect)
        elem_type.extend([block.element] * len(block.connect))
        for xel in block.elements:
            elem_map[xel] = iel
            iel += 1
    connect = np.array(connect, dtype=int)
    node_label = sorted(node_map.keys(), key=lambda k: node_map[k])
    elem_label = sorted(elem_map.keys(), key=lambda k: elem_map[k])
    f = vtk_file(jobid, mode="w")
    f.put_init(coords, node_label, elem_label, elem_type, connect)
    if disp is None:
        f.write_vtu_file(**kwds)
    else:
        kw = dict([(key, np.zeros_like(val)) for (key, val) in kwds.items()])
        f.snapshot(**kw)
        f.snapshot(disp=disp, **kwds)


def read(filename, disp=0):
    f = vtk_file(filename=filename, mode="r")
    if disp:
        return f.coords, f.node_label, f.connect, f.elem_label, f.elem_type

    # Create node and element tables
    nodtab = [[f.node_label[i]] + xc.tolist() for i, xc in enumerate(f.coords)]

    eletab = []
    for (e, et) in enumerate(f.elem_type):
        eledef = [f.elem_label[e], et] + f.connect[e].tolist()
        eletab.append(eledef)

    return nodtab, eletab


def test_write_fe_results():
    from distmesh import drectangle, distmesh2d, huniform

    np.random.seed(190)  # Always the same results
    fd = lambda p: drectangle(p, -1, 1, -1, 1)
    fh = huniform
    coords, connect = distmesh2d(
        fd, fh, 0.1, (-1, -1, 1, 1), [(-1, -1), (-1, 1), (1, -1), (1, 1)]
    )
    jobid = "Job"
    node_label = range(coords.shape[0])
    node_map = dict([(n, n) for n in node_label])
    elem_label = range(connect.shape[0])
    elem_map = dict([(n, n) for n in elem_label])
    elem_type = [element.C2D3] * connect.shape[0]
    scal = np.random.rand(coords.shape[0])
    vect = np.random.rand(coords.shape[0] * 2).reshape(-1, 2)
    tens = np.random.rand(connect.shape[0] * 9).reshape(-1, 9)
    symt = np.random.rand(connect.shape[0] * 6).reshape(-1, 6)
    kwds = dict(scal=scal, vect=vect, tens=tens, symt=symt)
    disp = np.zeros_like(coords)
    disp[:, 0] = 1
    snapshot(jobid, coords, node_map, elem_map, elem_type, connect, disp=disp, **kwds)
    filename = jobid + ".vtu"
    write_vtu_mesh(
        filename, coords, node_label, elem_label, elem_type, connect, check=1
    )
    os.remove(filename)


if __name__ == "__main__":
    # read('uniform_plate_tri_0.05.vtu')
    test_write_fe_results()
