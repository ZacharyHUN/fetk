import IPython
from pygments import highlight
from pygments.formatters import HtmlFormatter
from pygments.lexers import PythonLexer, YamlLexer


def display_html(file):

    with open(file) as fh:
        code = fh.read()

    formatter = HtmlFormatter()
    lexer = YamlLexer if file.endswith(".yaml") else PythonLexer

    return IPython.display.HTML(
        '<style type="text/css">{}</style>{}'.format(
            formatter.get_style_defs(".highlight"), highlight(code, lexer(), formatter)
        )
    )
