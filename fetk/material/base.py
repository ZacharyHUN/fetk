class base:
    params = None

    def __init__(self, parameters, density=None):
        self._set_parameters(parameters)
        self.density = density or 1.

    def _set_parameters(self, parameters):
        mytype = type(self).__name__
        keys = [_.lower() for _ in list(parameters.keys())]
        values = list(parameters.values())
        for (name, details) in self.params.items():
            default = details["default"]
            name1 = name.lower()
            name2 = name1.replace("_", " ")
            if name1 in keys:
                i = keys.index(name1)
                setattr(self, name, values[i])
                keys[i] = None
                continue
            elif name2 in keys:
                i = keys.index(name2)
                setattr(self, name, values[i])
                keys[i] = None
                continue
            for alias in details["aliases"]:
                if alias in keys:
                    i = keys.index(alias.lower())
                    setattr(self, name, values[i])
                    keys[i] = None
                    break
            else:
                if default is None:
                    raise ValueError(f"{mytype}: expected {name} to be defined")
                setattr(self, name, default)

    def stiffness(self, *args, **kwargs):
        raise NotImplementedError
