import numpy as np
from .base import base


class thermal(base):
    params = {
        "k": {"aliases": ("conductivity",), "default": None},
        "h": {"aliases": ("convection coefficient",), "default": 1.0},
    }

    def conductivity(self, coords):
        dimension = 1 if np.isscalar(coords) else coords.shape[1]
        if dimension == 1:
            return self.k
        return np.eye(dimension) * self.k
