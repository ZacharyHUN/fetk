from .elastic import elastic
from .thermal import thermal

_models = {"elastic": elastic, "thermal": thermal}


def factory(model, params):
    """Factory method for creating a new material

    Parameters
    ----------
    model : str
        The material model name matching one of the available material models
    id : int
        Integer ID of the material
    name : str
        The user given material name, eg, "Steel"
    params : dict
        Material model parameters

    """
    material = _models.get(model.lower())
    if material is None:
        raise ValueError(f"{model}: no such material model")
    return material(params)
